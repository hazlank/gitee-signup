var ErrorPrompt = {
    register_email: '邮箱格式有误',
    register_captcha: '请输入6位验证码',
    register_password: '密码至少六位',
    register_domain: '地址只允许字母、数字或者下划线（_）、中划线（－），至少 2 个字符，必须以字母开头，不能以特殊字符结尾'
}
var Validator = VeeValidate.Validator
var validation = {
    email (value) {
        var regex = /^\s*[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+\s*$/
        return new Promise(resolve => {
            resolve({
                valid: regex.test(value),
            })
        })
    },
    captcha (value) {
        var regex = /^.{6}$/
        return new Promise(resolve => {
            resolve({
                valid: regex.test(value),
            })
        })
    },
    domain (value) {
        var regex = /^[a-zA-Z][\w\_\-]*([a-zA-Z\d])$/
        return new Promise(resolve => {
            resolve({
                valid: regex.test(value),
            })
        })
    },
    password (value) {
        var regex = /^.{6,}$/
        return new Promise(resolve => {
            resolve({
                valid: regex.test(value),
            })
        })
    }
}
var getMessage = (field, params, data) => {
    return ErrorPrompt[field]
}

Object.keys(validation).forEach((key) => {
    Validator.extend(key, {
        ...{ getMessage: getMessage },
        ...{ validate: validation[key] }
    })
})


Vue.use(VeeValidate)
var a =new Vue({
    el: '#form',
    template: `
    <form class="gitee-register__form">
        <div class="gitee-register__input">
            <label for="register_email">邮箱</label>
            <input v-on:input="debounceInput" name='email'
            :class="{'is-error': errors.has('register_email')}" 
            placeholder="请输入工作邮箱"
            id='register_email'
            >
            <i :style="'color:'+(errors.first('register_email') ? 'red' : 'green')">{{ field.email ? (errors.first('register_email') ? 'X' : '√') : '' }}</i>
            <input
                class="none"
                v-validate="'email'"
                name="register_email"
                type="text"
                v-model="field.email"
            >
            <div class="gitee-validate__error">{{ errors.first('register_email') }}</div>
        </div>
        <div class="gitee-register__input">
            <label for="register_captcha">验证码</label>
            <div class='inline-flex'>
                <input v-on:input="debounceInput" name='captcha'
                :class="{'is-error': errors.has('register_captcha')}" 
                id='register_captcha'
                >
                <input
                class="none"
                name="register_captcha"
                type="text"
                v-validate="'captcha'" 
                v-model="field.captcha"
                >
                <img :src="'https://gitee.com/rucaptcha/'+ImgRandom" @click="ChangeImgRandom" class="pointer" width="140" height="40">
            </div>
            <i :style="'color:'+(errors.first('register_captcha') ? 'red' : 'green')">{{ field.captcha ? (errors.first('register_captcha') ? 'X' : '√') : '' }}</i>
            <div class="gitee-validate__error">{{ errors.first('register_captcha') }}</div>
        </div>
        <div class="gitee-register__input">
            <label for="register_email_captcha">邮箱验证</label>
            <div class="inline-flex">
                <input id="register_email_captcha" name="register_email_captcha" type="text" placeholder="请输入邮箱验证码">
                <button @click="CountDown" class="ui basic orange button" type="button" style="border-radius: 0;">{{ timer }}</button>
            </div>
            <a href="###">收不到邮件?</a>
        </div>
        <div class="gitee-register__input">
            <label for="register_name">姓名</label>
            <input name="register_name" id="register_name" type="text">
        </div>
        <div class="gitee-register__input relative">
            <label for="register_domain">个性域名</label>
            <div class="inline-flex">
                <div class="gitee-register__domain">https://gitee.com/</div>
                <input
                v-on:input="debounceInput"
                name='domain'
                :class="{'is-error': errors.has('register_domain')}" 
                id='register_domain'
                >
                <input
                class="none"
                name="register_domain"
                v-validate="'domain'"
                v-model="field.domain"
                type="text"
                >
            </div>
            <i :style="'color:'+(errors.first('register_domain') ? 'red' : 'green')">{{ field.domain ? (errors.first('register_domain') ? 'X' : '√') : '' }}</i>
            <span class="pointer gitee-register__question">?</span>
            <div class="ui left pointing  basic label">
                <ul>
                    <li>个人主页的地址，个性域名只可更改一次</li>
                    <li>个性域名 是用户在码云上的唯一标识，在开通码云账号时填写</li>
                    <li>您可以将个性域名分享给朋友，让他们通过你的个性域名访问你的码云主页</li>
                    <li>每个账号只能对应一个个性域名，建议您在注册时为您的码云账户精心起一个好名字</li>
                </ul>
            </div>
            <div class="gitee-validate__error">{{ errors.first('register_domain') }}</div>
        </div>
        <div class="gitee-register__input">
            <label for="register_password">密码</label>
            <div class="inline-flex gitee-register__encryption">
                <input
                id="register_password"
                name="register_password"
                :type="password_hidden ? 'text' : 'password'"
                v-validate="'password'" 
                :class="{'is-error': errors.has('register_password')}" 
                :data-vv-delay="delay"
                placeholder="密码不少于6位">
                <div @click="password_hidden=!password_hidden" class="pointer">{{ password_hidden ? '明文' : '不明文' }}</div>
            </div>
            <div class="gitee-validate__error">{{ errors.first('register_password') }}</div>
        </div>
        <div class="gitee-register__agree">
            <span @click="agree=!agree" class="pointer">
                <i>{{ agree ? '√' : ''}}</i>已阅读并同意
            </span>
            <span><a href="###">使用条款</a> 以及 <a href="###">非活跃帐号处理规范</a></span>
        </div>
        <div class="center">
            <button class="ui orange submit button large" id="submit" name="button" type="submit" style="width:300px;">注 册</button>
        </div>
    </form>
    `,
    data: () => {
        return {
            ImgRandom: '',
            field: {
                email: '',
                captcha: '',
                email_captcha: '',
                name: '',
                domain: '',
                password: ''
            },
            agree: false,
            password_hidden: true,
            times: 10,
            second: -1,
            resend: false,
            delay: 1000
        }
    },
    methods: {
        ChangeImgRandom () {
            this.ImgRandom = `?${Math.random()}`
        },
        debounceInput: _.debounce(function (e) {
            this.field[e.target.name] = e.target.value;
          }, 1000)
        ,
        CountDown () {
            if (this.second >= 0) {
                return
            }
            this.second = this.times
            this.resend = true
            var timer = setInterval(() => {
                this.second--
                if (this.second < 0) {
                clearInterval(timer)
                }
            }, 1000)
        }
    },
    computed: {
        timer () {
            return this.resend ? (this.second < 0
            ? '重新发送'
            : `${this.second}s`) : '获取验证码'
        }
    }
})